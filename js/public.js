$( document ).ready(function() {
    const months = ["January", "Februari", "Maret", "April", "Mei", "Juni",
                    "Juli", "Agustus", "September", "Oktober", "November", "Desember"];

    let date = new Date();
    let month = months[date.getMonth()];

    let datetime = date.getDate() + ' ' + month + ' ' + date.getFullYear();

    $('.datetime').html(datetime);
});