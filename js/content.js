function showPost(id) {
    $('#section-dashboard').addClass('hide');
    $('#section-about').addClass('hide');
    $('#section-profile').addClass('hide');

    $('#section-post').removeClass('hide');
    $('#post1').addClass('hide');
    $('#post2').addClass('hide');
    $('#post3').addClass('hide');
    $('#'+id).removeClass('hide');
    $('#sidebar').removeClass('hide');
    $('#bottombar').removeClass('hide');
}