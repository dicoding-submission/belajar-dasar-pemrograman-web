$(document).ready(function() {
    function bottomlist() {
        const links = ['post1', 'post2', 'post3', 'article_4', 'article_5',
                       'arrticle_6', 'article_7', 'article_8', 'article_9', 'article_10',
                       'arrticle_11', 'article_12', 'article_13', 'article_14', 'article_15'];
        const lists = ['Kucing', 'Virtual Reality', 'Gliese 667Cc', 'Article 4 (Kosong)', 'Article 5 (Kosong)',
                       'Article 6 (Kosong)', 'Article 7 (Kosong)', 'Article 8 (Kosong)', 'Article 9 (Kosong)', 'Article 10 (Kosong)',
                       'Article 11 (Kosong)', 'Article 12 (Kosong)', 'Article 13 (Kosong)', 'Article 14 (Kosong)', 'Article 15 (Kosong)'];
        let result = '';
        for (let x = 0; x < lists.length; x++) {
            result += renderBottomList(lists[x], links[x]);
        }
        return result;
    }
    
    function renderBottomList(item1, item2) {
        return `<a href="#${item2}" onClick="${item2}">${item1}</a> `;
    }
    
    $('#bottomarticlelist').html(bottomlist());
});