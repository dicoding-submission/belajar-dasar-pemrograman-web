$('#nav-dashboard').click(function(){
    $('#section-dashboard').removeClass('hide');
    $('#section-about').addClass('hide');
    $('#section-profile').addClass('hide');

    $('#section-post').addClass('hide');
    $('#post1').addClass('hide');
    $('#post2').addClass('hide');
    $('#post3').addClass('hide');
    $('#sidebar').addClass('hide');
    $('#bottombar').addClass('hide');
});

$('#nav-about').click(function(){
    $('#section-dashboard').addClass('hide');
    $('#section-about').removeClass('hide');
    $('#section-profile').addClass('hide');

    $('#section-post').addClass('hide');
    $('#post1').addClass('hide');
    $('#post2').addClass('hide');
    $('#post3').addClass('hide');
    $('#sidebar').addClass('hide');
    $('#bottombar').addClass('hide');
});

$('#nav-profile').click(function(){
    if($('#user').hasClass('hide')) {
        $('#user').removeClass('hide')
    }
    else {
        $('#user').addClass('hide')
    }
});

function showProfile() {
    $('#section-dashboard').addClass('hide');
    $('#section-about').addClass('hide');
    $('#section-profile').removeClass('hide');

    $('#section-post').addClass('hide');
    $('#post1').addClass('hide');
    $('#post2').addClass('hide');
    $('#post3').addClass('hide');
    $('#sidebar').addClass('hide');
    $('#bottombar').addClass('hide');
}